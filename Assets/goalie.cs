﻿using UnityEngine;
using System.Collections;

public class goalie : MonoBehaviour
{

	//make a list of animations
	enum animationState
	{
		SQUAT_LEFT,
		SQUAT_RIGHT,
		OVERHEAD,
		AERIAL_LEFT,
		AERIAL_RIGHT,
		MID_AIR1,
		MID_AIR2,
		THROW,
		THROW_2
	};

	public worldhandler handler = null;

	animationState currentAnimationState;
	string animationName = "GSP_animations";
	Animation goalieAnimation;
	//int totalFrames = 1059;
	public float goalieSpeed;
	float currentTime = 0.0f;

	bool jumping = false;

	float squatLeftStart;// = 81;
	float squatLeftEnd;// = 141;

	float squatRightStart;// = 142;
	float squatRightEnd;// = 202;

	float overheadStart;// = 203;
	float overheadEnd;// = 273

	float aerialLeftStart;// = 274
	float aerialLeftEnd;// = 394

	float aerialRightStart;// = 395
	float aerialRightEnd;// = 515

	float mid1Start;// = 516
	float mid1End;// = 636

	float mid2Start;// = 637
	float mid2End;// = 757

	float throw1Start;// = 758
	float throw1End;// = 934

	float throw2Start;// = 935
	float throw2End;// = 1059

	Rigidbody body;
	public float upForce = 100.0f;
	public float goalieForce = 100.0f;
	public bool caughtBall = false;

	Vector3 beginningPosition = new Vector3();

	//make the character lerp from side to side during dive?
	//
	public GameObject leftHand;
	public GameObject rightHand;


	// Use this for initialization
	void Start()
	{
		beginningPosition = transform.position;
		body = GetComponent<Rigidbody>() as Rigidbody;
		goalieAnimation = GetComponent<Animation>();
		float frameDelta = (1.0f / goalieAnimation.clip.frameRate);

		leftHand = GameObject.FindGameObjectWithTag("Left Hand");
		rightHand = GameObject.FindGameObjectWithTag("Right Hand");

		squatLeftStart = frameDelta * 81;
		squatLeftEnd = frameDelta * 141;
		squatRightStart = frameDelta * 142;
		squatRightEnd = frameDelta * 202;
		overheadStart = frameDelta * 203;
		overheadEnd = frameDelta * 273;
		aerialLeftStart = frameDelta * 274;
		aerialLeftEnd = frameDelta * 394;
		aerialRightStart = frameDelta * 395;
		aerialRightEnd = frameDelta * 515;
		mid1Start = frameDelta * 516;
		mid1End = frameDelta * 636;
		mid2Start = frameDelta * 637;
		mid2End = frameDelta * 757;
		throw1Start = frameDelta * 758;
		throw1End = frameDelta * 934;
		throw2Start = frameDelta * 935;
		throw2End = frameDelta * 1059;
		Reset();
	}

	public void KeepBallInHands(Ball gameBall)
	{
		//disable physics components of ball and put it in between the hands
		Vector3 directionBtoA = leftHand.transform.position - rightHand.transform.position; // directionCtoA = positionA - positionC
		Vector3 directionAtoB = rightHand.transform.position - leftHand.transform.position; // directionCtoB = positionB - positionC
		Vector3 midpointAtoB = new Vector3((directionBtoA.x + directionAtoB.x) / 2.0f, (directionBtoA.y + directionAtoB.y) / 2.0f, (directionBtoA.z + directionAtoB.z) / 2.0f); // midpoint between A B this is what you want

		gameBall.transform.position = midpointAtoB;
	}

	public void Reset()
	{
		//currentTime = 0.0f;
		//goalieAnimation.Play();

		//goalieAnimation.Stop();
		transform.position = beginningPosition;
		//on start give it a 50/50 chance which direction to start in
		int direction = Random.Range(0, 2);
		if (direction == 0)
		{
			SetAnimation(animationState.SQUAT_LEFT);
		}

		else
		{
			SetAnimation(animationState.SQUAT_RIGHT);
		}

		goalieAnimation.Stop();
	}

	void SetAnimation(animationState newAnimation)
	{
		//squat left to squat right
		currentAnimationState = newAnimation;
		goalieAnimation.Stop();

		switch (newAnimation)
		{
			case animationState.SQUAT_LEFT:
				{
					goalieAnimation[animationName].time = squatLeftStart;
					break;
				}

			case animationState.SQUAT_RIGHT:
				{
					goalieAnimation[animationName].time = squatRightStart;
					break;
				}

			case animationState.AERIAL_LEFT:
				{
					goalieAnimation[animationName].time = aerialLeftStart;
					break;
				}

			case animationState.AERIAL_RIGHT:
				{
					goalieAnimation[animationName].time = aerialRightStart;
					break;
				}

			case animationState.OVERHEAD:
				{
					goalieAnimation[animationName].time = overheadStart;
					break;
				}

			case animationState.MID_AIR1:
				{
					goalieAnimation[animationName].time = mid1Start;
					break;
				}

			case animationState.MID_AIR2:
				{
					goalieAnimation[animationName].time = mid2Start;
					break;
				}
		}

		currentTime = goalieAnimation[animationName].time;
		goalieAnimation.Play(animationName);
	}

	// Update is called once per frame
	void Update()
	{
		if (!handler.gamePaused)
		{
			if(currentAnimationState == animationState.SQUAT_LEFT || currentAnimationState == animationState.SQUAT_RIGHT)
			{
				//capsule.direction = 1;
				goalieAnimation[animationName].speed = 0.25f;
			}

			else
			{
				//capsule.direction = 0;
				goalieAnimation[animationName].speed = 1.0f;
			}
			currentTime = goalieAnimation[animationName].time;

			//if the player clicks the mouse then 
			PlayAnimation(currentAnimationState);
		}

		else
		{
			goalieAnimation.Stop();
		}
	}

	public void Leap()
	{
		//pick between one of 4 save animations and move the goalie accordingly
		int anim = Random.Range(1, 101);
		if(anim > 0 && anim <= 15)
		{
			SetAnimation(animationState.AERIAL_LEFT);
			body.AddForce(transform.right * goalieForce);
		}

		else if(anim > 15 && anim <= 30)
		{
			SetAnimation(animationState.AERIAL_RIGHT);
			body.AddForce(-transform.right * goalieForce);
		}

		else if(anim > 30 && anim <= 50)
		{
			SetAnimation(animationState.OVERHEAD);
			body.AddForce(transform.up * upForce);
		}

		else if(anim > 50 && anim <= 75)
		{
			SetAnimation(animationState.MID_AIR1);
			body.AddForce((transform.right * goalieForce) + (transform.up * upForce));
		}

		else
		{
			SetAnimation(animationState.MID_AIR2);
			body.AddForce((-transform.right * goalieForce) + (transform.up * upForce));
		}
	}

	void PlayAnimation(animationState currentState)
	{
		switch (currentState)
		{
			case animationState.SQUAT_LEFT:
				{
					if (currentTime >= squatLeftStart && currentTime <= squatLeftEnd)
					{
						//while the goalie is animating to the left, move him at the appropriate speed
						transform.position -= transform.right * goalieSpeed;
						goalieAnimation.Play(animationName);
					}

					else
					{
						SetAnimation(animationState.SQUAT_RIGHT);
					}
					break;
				}

			case animationState.SQUAT_RIGHT:
				{
					if (currentTime >= squatRightStart && currentTime <= squatRightEnd)
					{
						//while the goalie is animating to the left, move him at the appropriate speed
						transform.position += transform.right * goalieSpeed;
						goalieAnimation.Play(animationName);
					}

					else
					{
						SetAnimation(animationState.SQUAT_LEFT);
					}
					break;
				}

			case animationState.AERIAL_LEFT:
				{
					//throw the goalie to the upper left
					if (currentTime >= aerialLeftStart && currentTime <= aerialLeftEnd)
					{
						goalieAnimation.Play(animationName);
					}

					else
					{
						goalieAnimation.Stop();
					}

					break;
				}

			case animationState.AERIAL_RIGHT:
				{
					//throw the goalie to the upper right
					if (jumping)
					{
						//throw the goalie to the upper left
						if (currentTime >= aerialRightStart && currentTime <= aerialRightEnd)
						{
							goalieAnimation.Play(animationName);
						}

						else
						{
							goalieAnimation.Stop();
						}
					}

					else
					{
						//toss the goalie
						jumping = true;
					}
					break;
				}

			case animationState.OVERHEAD:
				{
					//throw the goalie up a bit
					if (jumping)
					{
						//throw the goalie to the upper left
						if (currentTime >= overheadStart && currentTime <= overheadEnd)
						{
							goalieAnimation.Play(animationName);
						}
						else
						{
							goalieAnimation.Stop();
						}
					}

					else
					{
						//toss the goalie
						jumping = true;
					}
					break;
				}

			case animationState.MID_AIR1:
				{
					//need to see what direction this is first!
					if (jumping)
					{
						//throw the goalie to the upper left
						if (currentTime >= mid1Start && currentTime <= mid1End)
						{
							goalieAnimation.Play(animationName);
						}
						else
						{
							goalieAnimation.Stop();
						}
					}

					else
					{
						//toss the goalie
						jumping = true;
					}
					break;
				}

			case animationState.MID_AIR2:
				{
					//need to see what direction this is first!
					if (jumping)
					{
						//throw the goalie to the upper left
						if (currentTime >= mid2Start && currentTime <= mid2End)
						{
							goalieAnimation.Play(animationName);
						}
						else
						{
							goalieAnimation.Stop();
						}
					}

					else
					{
						//toss the goalie
						jumping = true;
					}
					break;
				}
		}
	}

	public void StopAnimating()
	{
		goalieAnimation.Stop();
	}
}
